/*
*   function Class( args ) {}       // Class
*   var Function = function() {}    // Function
*       (function() {})();          // Function which will be executed on script run
*   var Object = {}                 // Object
*/

// Create the canvas element
var canvas = document.createElement("canvas");  // Create the Canvas element
var ctx = canvas.getContext("2d");              // Set the canvas' context to 2D graphics (3D would use OpenGL)
canvas.width = 600;                             // Set the canvas width
canvas.height = 480;                            // Set the canvas height
document.body.appendChild(canvas);              // Append the Canvas element to the page's DOM

// Global variables
var start_time = Date.now();    // The starting point of the game (for game ticks and pet age)
var elapsed = 0;                // Elapsed time (in seconds)
var time_passed = 0;            // Used for game ticks
var then = Date.now();          // Used for game ticks
var randTime = false;           // Used for game ticks
var pet = {};                   // Globalise the Pet object

// Animation / interaction global blocks
var petAnimation;
var petAnimationBlock;
var interactionBlock = false;

// Game settings object
var game_settings = {

    // Pet attribute settings
    pet : {
        health: {
            start: 100,
            max: 150,
            addition_rate: 0,
            reduction_rate: 0
        },
        happiness: {
            start: 100,
            max: 100,
            addition_rate: 0,
            reduction_rate: -0.15
        },
        hunger: {
            start: 100,
            max: 100,
            addition_rate: 0,
            reduction_rate: -0.2
        },
        money: {
            start: 100,
            max: 1000,
            addition_rate: 0.2,
            reduction_rate: 0
        }
    },

    // User interactions
    actions : {

        feed : {
            cost: 20,
            attribute: 'hunger',
            effect: 35,
            callback: function() { pet.feed(); }
        },

        toy : {
            cost: 25,
            attribute: 'happiness',
            effect: 25,
            callback: function() { pet.play(); }
        }
    }
};

// UI elements (action buttons)
var UIElements = {
    // Feed button
    feedBtn: {
        x: 20,                              // X coordinates of button (top left of button)
        y: (canvas.height - 70),            // Y coordinates (60 pixels from the bottom of the canvas)
        height: 33,                         // Button height
        width: 100,                          // Button width
        action: game_settings.actions.feed, // The action triggered by clicking the button
        duration: 4000,                     // The duration which the resulting action will take (further interactions will be blocked for this time)
        content : {
            image : 'images/game/button-sprite.png',
            x : 0,
            y : 0
        }
    },
    // Play button
    playBtn: {
        x: 190,                             // X coordinates of button (top left of button)
        y: (canvas.height - 70),            // Y coordinates (60 pixels from the bottom of the canvas)
        height: 33,                         // Button height
        width: 100,                          // Button width
        action: game_settings.actions.toy,  // The action triggered by clicking the button
        duration: 6000,                     // The action triggered by clicking the button
        content : {
            image : 'images/game/button-sprite.png',
            x : 0,
            y : 33
        }
    }
};


// Load images
(function() {
    var resourceCache = {};     // Setup the final resource cache object
    var loading = [];           // Array of images being loaded
    var readyCallbacks = [];    // Actions to take on completion

    // Load an image url or an array of image urls
    function load(urlOrArr) {
        if(urlOrArr instanceof Array) {
            urlOrArr.forEach(function(url) {
                _load(url);
            });
        }
        else {
            _load(urlOrArr);
        }
    }
    // Load the image from the supplied URL
    function _load(url) {
        // If we've already loaded this image, skip it
        if(resourceCache[url]) {
            return resourceCache[url];
        }
        // Otherwise..
        else {
            var img = new Image();          // Setup a new image object
            img.onload = function() {       // Set up the action to take once the image is loaded..
                resourceCache[url] = img;   // Add the image to our temp loading array

                // Once loaded, trigger any callback functions
                if(isReady()) {
                    readyCallbacks.forEach(function(func) { func(); });
                }
            };
            resourceCache[url] = false;  // Default the image's entry in the cache so we know if it failed
            img.src = url;               // Attempt to load the image!
        }
    }
    // Gets a specified image from the resource cache
    function get(url) {
        return resourceCache[url];
    }
    // Checks if all the requested images have been loaded
    function isReady() {
        var ready = true;   // Default to "yeah totally loaded"
        // For every image in the cache..
        for(var k in resourceCache) {
            // Check the image is actually loaded (see ln 145 above for example of an image not loading)
            if(resourceCache.hasOwnProperty(k) &&
               !resourceCache[k]) {
                ready = false;  // We aen't ready yet!
            }
        }
        return ready;
    }
    // Callbacks to trigger once an image has been loaded
    function onReady(func) {
        readyCallbacks.push(func);
    }
    // Load dem resources
    window.resources = {
        load: load,
        get: get,
        onReady: onReady,
        isReady: isReady
    };
})();

// Images to load
resources.load([
    'images/game/icon-sprite.png',
    'images/game/button-sprite.png',
    'images/pet-sprite.png',
    'images/background.jpg'
]);
resources.onReady(init);


/*
*   Sprite class
*   @param  string  url     The sprite's url (to pull from the local resource cache) (e.g. 'images/sprite.png')
*   @param  array   pos     The starting frame's position in the sprite sheet (e.g. [0,0])
*   @param  array   size    The height and width of each frame (e.g. [100,75])
*   @param  int     speed   The speed at which to change frames (FPS) (e.g. 30)
*   @param  int     frames  The sequence of frames to render (loops to the beginning of the array) (e.g. [0,1,2,1])
*   @param  string  dir     The direction in which the animation's frames are, defaults to 'horizontal' (e.g. 'vertical')
*   @param  int     once    Whether or not to loop the animation's frames
*/
function Sprite(url, pos, size, speed, frames, dir, once) {
    this.pos = pos;
    this.size = size;
    this.speed = typeof speed === 'number' ? speed : 0;
    this.frames = frames;
    this._index = 0;
    this.url = url;
    this.dir = dir || 'horizontal';
    this.once = once;
};
// Update the sprite index (which sprite to use next in it's sheet)
Sprite.prototype.update = function(dt) {
    this._index += this.speed*dt;
}
// Render dat sprite
Sprite.prototype.render = function(ctx) {

    var frame;

    // If a speed is set, calculate the next frame to render
    if(this.speed > 0) {
        var max = this.frames.length;       // Get the number of frames in a sequence
        var idx = Math.floor(this._index);  // The desired index
        frame = this.frames[idx % max];     // Calculate which frame to render next (calculated based on the speed and index)

        // If we're only doing the animation sequence once (not looping) and we're at the end of the frame sequence, finish
        if(this.once && idx >= max) {
            this.done = true;
            return;
        }
    }
    // If not, we're only rendering one frame
    else {
        frame = 0;
    }

    // Get the X and Y coordinates of the frame
    var x = this.pos[0];
    var y = this.pos[1];

    // Change the position of the frame based on if we're going horizontally or vertically
    if(this.dir == 'vertical') {
        y += frame * this.size[1];
    }
    else {
        x += frame * this.size[0];
    }
    // All set, let's render this frame!
    ctx.drawImage(resources.get(this.url),
                  x, y,
                  this.size[0], this.size[1],
                  110, 150,
                  this.size[0], this.size[1]);
}


/*
*   Pet class
*   @param  string  name        The pet's name
*   @param  string  petSprite   The sprite to use (in case we were going to have multiple pets to choose from)
*/
function Pet(name, petSprite) {
    this.name = name;
    this.sprite = new Sprite(petSprite.url, petSprite.pos, petSprite.size, petSprite.speed, petSprite.frames);
    this.health = game_settings.pet.health.start;
    this.happiness = game_settings.pet.happiness.start;
    this.hunger = game_settings.pet.hunger.start;
    this.age = 0;
    this.money = game_settings.pet.money.start;
    this.state = 'normal';

    this.animations = {
        normal : {
            url: 'images/pet-sprite.png',
            pos: [0, 0],
            size: [256, 177],
            speed: 1,
            frames: [0, 1]
        },
        feed : {
            url: 'images/pet-sprite.png',
            pos: [0, 177],
            size: [256, 177],
            speed: 1,
            frames: [0, 1],
            duration: 4000
        },
        play : {
            url: 'images/pet-sprite.png',
            pos: [0, 354],
            size: [256, 177],
            speed: 1,
            frames: [0, 1, 2, 1],
            duration: 6000
        },
        sad : {
            url: 'images/pet-sprite.png',
            pos: [0, 531],
            size: [256, 177],
            speed: 1,
            frames: [0, 1]
        },
        sick : {
            url: 'images/pet-sprite.png',
            pos: [0, 708],
            size: [256, 177],
            speed: 1,
            frames: [0, 1]
        }
    }
}
// Prints a supplied string via the console
Pet.prototype.speak = function( text ) {
    console.log(text);
}
// The pet's normal state
Pet.prototype.normal = function() {
    pet.animate('normal');
}
// The pet's feed action
Pet.prototype.feed = function() {
    pet.speak("om nom nom");
    pet.animate('feed');
}
// The pet's play action
Pet.prototype.play = function() {
    pet.speak("squeee!");
    pet.animate('play');
}
// The pet's sad state ;___;
Pet.prototype.sad = function() {
    pet.speak(":(");
    pet.animate('sad');
}
// The pet's sick state
Pet.prototype.sick = function() {
    pet.speak("bleeeeh");
    pet.animate('sick');
}
/*
*   Process an interaction with the pet
*   @param  object  actionObj   The action object to be processed (uses the games_settings.actions object)
*   e.g.:
*   {
*       cost: 20,
*       attribute: 'hunger',
*       effect: 40,
*       callback: function() { pet.feed(); }
*   }
*/
Pet.prototype.interaction = function( actionObj ) {
    // If the object contains all the necessary attributes..
    if('attribute' in actionObj && 'cost' in actionObj && 'effect' in actionObj)
    {
        // Check if the pet / player has enough money for this, if so..
        if(pet.money >= actionObj.cost)
        {
            // If the effected attribute is less than it's set max..
            if( Math.round(pet[actionObj.attribute]) < game_settings.pet[actionObj.attribute].max )
            {
                // Deduct the cost from the player's money
                this.updateAttribute('money', pet.money - actionObj.cost);
                // Update the affected attribute's value
                this.updateAttribute(actionObj.attribute, pet[actionObj.attribute] + actionObj.effect);

                // If the action object has a callback function, trigger it
                if('callback' in actionObj) {
                    actionObj.callback();
                }
            }
        }
        // If they're too poor, let them know
        else {
            console.log("You don't have enough money for that, it costs " + actionObj.cost + "!");
        }
    }
}
/*
*   Checks if the pet's state needs to change (to allow transitions between states, rather than hammering them every tick)
*/
Pet.prototype.checkChangeState = function( state ) {

    if( state != pet.state ) {
        pet.state = state;
        pet[state]();
    }
}
/*
*   Contains a bunch of checks for various attributes, and requests to change the pet's state based on their outcomes
*/
Pet.prototype.updateState = function() {

    if( pet.health < 50 ) {
        pet.checkChangeState('sick');
    }
    else if( pet.happiness < 45 ) {
        pet.checkChangeState('sad');
    }
    else {
        pet.checkChangeState('normal');
    }
}
/*
*   Update one of the pet's attributes
*   @param  string  attribute   The attribute to update (e.g. 'hunger')
*   @param  int     value       The amount to set the attribute to (e.g. 100)
*/
Pet.prototype.updateAttribute = function(attribute, value) {

    // If the request attribute exists in both the pet's object and is defined in the pet's settings (and has a set max value)
    if(attribute in pet &&
        attribute in game_settings.pet &&
        'max' in game_settings.pet[attribute])
    {
        // If the requested value is greater than the set max, use the max value instead (so the value never exceeds the max)
        if(value > game_settings.pet[attribute]['max']) {
            pet[attribute] = game_settings.pet[attribute]['max'];
        }
        // If the value is below 0, set it to 0 instead (so the value never becomes a negative number)
        else if (value < 0) {
            pet[attribute] = 0;
        }
        // Otherwise, update the attribute to the requested value
        else {
            pet[attribute] = value;
        }
    }
}
// Update the pet each tick
Pet.prototype.update = function() {

    // The attributes to update each tick
    var rates = ['health', 'happiness', 'hunger', 'money'];

    // Any rate adjustments
    if(pet.happiness > 0 && (pet.happiness + (game_settings.pet.happiness.addition_rate + game_settings.pet.happiness.reduction_rate)) <= 0) {
        game_settings.pet.health.reduction_rate -= 0.15;
    }
    if(pet.hunger > 0 && (pet.hunger + (game_settings.pet.hunger.addition_rate + game_settings.pet.hunger.reduction_rate)) <= 0) {
        game_settings.pet.health.reduction_rate -= 0.4;
    }

    // For each attribute to update..
    _.each(rates, function(attribute) {

        // Check the attribute exists in the pet object, and that they have a max and rate
        if(attribute in pet &&
            attribute in game_settings.pet &&
            'max' in game_settings.pet[attribute] &&
            'addition_rate' in game_settings.pet[attribute] &&
            'reduction_rate' in game_settings.pet[attribute])
        {
            // If so, calculate the attribute's updated value based on the rate, and request to update it
            var updatedVal = (pet[attribute] + (game_settings.pet[attribute]['addition_rate'] + game_settings.pet[attribute]['reduction_rate']));
            pet.updateAttribute(attribute, updatedVal);
        }
    });

    // Check and update the pet's state (happy, sad, sick etc)
    pet.updateState();
}
/*
*   Update the pet's current animation
*   @param  string  animation   The animation to use (e.g. 'happy')
*/
Pet.prototype.animate = function( animation ) {

    // If the animation is not blocked, clear the current animation
    if(!petAnimationBlock) {
        clearTimeout(petAnimation);
    }

    // Find the requested animation..
    if( animation in pet.animations ) {
        // Create an object with all the required sprite values
        var sprite = {
            url: pet.animations[animation].url,
            pos: pet.animations[animation].pos,
            size: pet.animations[animation].size,
            speed: pet.animations[animation].speed,
            frames: pet.animations[animation].frames
        };

        // Update the pet's animation sprite
        pet.sprite = new Sprite(sprite.url, sprite.pos, sprite.size, sprite.speed, sprite.frames);

        // If the animation isn't permanent (e.g. a feeding action rather than a state change such as "sad")
        if(pet.animations[animation].duration) {
            // Lock the animation for it's duration (so it can't be changed whilst it's running)
            petAnimationBlock = true;
            // Set a timeout to return to it's state's animation ('normal', 'sad' etc)
            petAnimation = setTimeout(function() {
            pet.sprite = new Sprite(pet.animations[pet.state].url, pet.animations[pet.state].pos, pet.animations[pet.state].size, pet.animations[pet.state].speed, pet.animations[pet.state].frames);
            // Unblock the animation
            petAnimationBlock = false;
            }, pet.animations[animation].duration); // The duration to play the animation for
        }
    }
}

// Set up dat pet
var setupPet = function() {
    // Set up the pet's sprite
    var petSprite = {
        url: 'images/pet-sprite.png',
        pos: [0, 0],
        size: [256, 177],
        speed: 1,
        frames: [0, 1]
    };
    // Create the pet
    pet = new Pet('Horace', petSprite);
}


// Reset the game (currentl not used)
var reset = function () {};


/*
*   Update the game this tick
*/
var update = function () {
    // Duration since the beginning of the game
    time_passed = (Date.now() - start_time) / 1000;
    // The actual seconds
    var seconds_passed = Math.floor(time_passed);

    // Update the pet's age
    pet.age = seconds_passed;

    // We want to update the game as a whole only every second (to reduce resource consumption)
    if(seconds_passed != elapsed) {
        elapsed = seconds_passed;   // Update the time passed in seconds
        pet.update();               // Update the pet
    }
    // However, we want to update the pet's animations whenever we like
    pet.sprite.update(0.15);
};

/*
*   Process any click interactions with the UI
*   @param  Object  e   The click event
*/
var processInteraction = function(e) {
    // Capture the click coordinates
    var clickX = e.clientX;
    var clickY = e.clientY;

    // If the interaction isn't blocked (to lock down the controls during an action's animation and sequence)
    if( !interactionBlock )
    {
        // Loop through each UI element
        _.each(UIElements, function(elem)
        {
            // Check if the click occured within each element
            if( (clickX > elem.x && clickX < elem.x + elem.width)
                && (clickY > elem.y && clickY < elem.y + elem.height) )
            {
                // If so, call it's action function
                pet.interaction( elem.action );
                // Block any further interactions
                interactionBlock = true;
                setTimeout(function() {
                    // Unlock the interaction once the current action's duration is up
                    interactionBlock = false;
                }, elem.duration);
            }
        });
    }
}

// Draw everything
var render = function () {
    // Render the background (just a white square currently)
    //ctx.fillStyle = "rgb(255, 255, 255)";
    //ctx.fillRect(0,0,512,480);
    ctx.clearRect( 0, 0, canvas.width, canvas.height );
    ctx.drawImage(resources.get('images/background.jpg'), 0, 0);

    // Render the pet
    pet.sprite.render(ctx);

    // Top stats bar
    ctx.fillStyle = "rgb(255, 255, 255)";
    ctx.fillRect(0,0,600,38);
    // Stats bar border
    ctx.beginPath();
    ctx.moveTo(0, 38);
    ctx.lineTo(600, 38);
    ctx.stroke();

    // Set out the color, font etc of UI elements
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.font = "18px Helvetica";
    ctx.textAlign = "left";
    ctx.textBaseline = "top";

    // Render the counters
    // Health
    ctx.drawImage(resources.get('images/game/icon-sprite.png'), 0, 0, 25, 25, 20, 5, 25, 25);
    ctx.fillText(Math.round(pet.health), 50, 8);

    // Happiness
    ctx.drawImage(resources.get('images/game/icon-sprite.png'), 25, 0, 25, 25, 180, 5, 25, 25);
    ctx.fillText(Math.round(pet.happiness), 210, 8);

    // Hunger
    ctx.drawImage(resources.get('images/game/icon-sprite.png'), 50, 0, 25, 25, 340, 5, 25, 25);
    ctx.fillText(Math.round(pet.hunger), 370, 8);

    // Money
    ctx.drawImage(resources.get('images/game/icon-sprite.png'), 75, 0, 25, 25, 500, 5, 25, 25);
    ctx.fillText(Math.round(pet.money), 530, 8);

    // Age
    //ctx.fillText("Age: " + pet.age + " seconds", 30, 30);

    // Render any UI elements
    _.each(UIElements, function(elem)
    {
        ctx.drawImage(resources.get(elem.content.image), elem.content.x, elem.content.y, elem.width, elem.height, elem.x, elem.y, elem.width, elem.height);
        /*
        ctx.rect(elem.x, elem.y, elem.width, elem.height);
        ctx.stroke();
        if(elem.hasOwnProperty('content')){
            ctx.fillText(elem.content.text, (elem.x + elem.content.insetX), (elem.y + elem.content.insetY));
        }
        */
    });
};

// The main game loop
var main = function () {
    var now = Date.now();
    var delta = now - then;
    //update(delta / 1000, randTime);
    update();   // Update the game
    render();   // Render everything

    then = now;
};

// Start the game!
function init() {
    canvas.addEventListener("click", processInteraction);   // Set up event listeners for user clicks on the canvas
    reset();                // Reset the game (currently not used)
    setupPet();             // Set up the pet
    then = Date.now();      // Set the game time counter
    setInterval(main, 100); // Execution speed
}
